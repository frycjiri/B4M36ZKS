<?php

namespace PageObjects
{
	/**
	 * LoginPage short summary.
	 *
	 * LoginPage description.
	 *
	 * @version 1.0
	 * @author Fry
	 */
	class LoginPage extends Page
	{
		public function login(LoginModel $model) : bool
		{
			if(!$this->elementExist(\Facebook\WebDriver\WebDriverBy::cssSelector('form[action="/login"]')))
				throw new \Exception("Wrong page");
			$this->inputText('form[action="/login"] input[name="username"]',$model->username);
			$this->inputText('form[action="/login"] input[name="password"]',$model->password);
			$this->Webdriver()->getKeyboard()->pressKey(\Facebook\WebDriver\WebDriverKeys::ENTER);
			$test=[$this->elementExist(\Facebook\WebDriver\WebDriverBy::linkText('Sign out')),$this->elementExist(\Facebook\WebDriver\WebDriverBy::cssSelector('a[href="/logout"][class="logout"]'))];
			return $test[0] && $test[1];
		}
	}
	class LoginModel
	{
		public $username,$password;
		public function setUsername(string $username) : LoginModel
		{
			$this->username=$username;
			return $this;
		}
		public function setPassword(string $password) : LoginModel
		{
			$this->password=$password;
			return $this;
		}
		public static function create() : LoginModel
		{
			return new LoginModel();
		}
	}
}