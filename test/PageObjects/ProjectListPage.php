<?php

namespace PageObjects
{
	/**
	 * ProjectListPage short summary.
	 *
	 * ProjectListPage description.
	 *
	 * @version 1.0
	 * @author Fry
	 */
	class ProjectListPage extends Page
	{

		public function project_count() : int
		{
			return count($this->Webdriver()->findElements(\Facebook\WebDriver\WebDriverBy::cssSelector('a.project.root.leaf')));
		}
		public function get_project(int $index) : ProjectListElement
		{
			return new ProjectListElement($this->Webdriver()->findElements(\Facebook\WebDriver\WebDriverBy::cssSelector('a.project.root.leaf'))[0],$this->Website());
		}

	}
	class ProjectListElement
	{
		public $name;
		public $link;
		private $element,$website;
		public function __construct(\Facebook\WebDriver\Remote\RemoteWebElement $element,$website)
		{
			$this->element=$element;
			$this->name=$element->getText();
			$this->link=$element->getAttribute('href');
			$this->website=$website;
		}
		public function open() : ProjectPage
		{
			$href=$this->element->getAttribute('href');
			$this->element->click();
			return new ProjectPage($this->website,$href);
		}
	}
}