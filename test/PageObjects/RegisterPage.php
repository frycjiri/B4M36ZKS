<?php

namespace PageObjects
{
	/**
	 * RegisterPage short summary.
	 *
	 * RegisterPage description.
	 *
	 * @version 1.0
	 * @author Fry
	 */
	class RegisterPage extends Page
	{

		public function register(RegisterModel $model) : bool
		{
			if(!$this->elementExist(\Facebook\WebDriver\WebDriverBy::cssSelector('form[action="/account/register"]')))
				throw new \Exception("Wrong page");
			$this->inputText('form[action="/account/register"] input#user_login',$model->username);
			$this->inputText('form[action="/account/register"] input#user_password',$model->password);
			$this->inputText('form[action="/account/register"] input#user_password_confirmation',$model->password);
			$this->inputText('form[action="/account/register"] input#user_firstname',$model->firstname);
			$this->inputText('form[action="/account/register"] input#user_lastname',$model->lastname);
			$this->inputText('form[action="/account/register"] input#user_mail',$model->mail);
			$this->Webdriver()->getKeyboard()->pressKey(\Facebook\WebDriver\WebDriverKeys::ENTER);
			$test=[$this->elementExist(\Facebook\WebDriver\WebDriverBy::linkText('Sign out')),$this->elementExist(\Facebook\WebDriver\WebDriverBy::cssSelector('a[href="/logout"][class="logout"]'))];
			return $test[0] && $test[1];
		}
	}
	class RegisterModel
	{
		public $username,$password,$mail,$firstname,$lastname;
		public function setUsername(string $username) : RegisterModel
		{
			$this->username=$username;
			return $this;
		}
		public function setPassword(string $password) : RegisterModel
		{
			$this->password=$password;
			return $this;
		}
		public function setFirstName(string $firstname) : RegisterModel
		{
			$this->firstname=$firstname;
			return $this;
		}
		public function setLastName(string $lastname) : RegisterModel
		{
			$this->lastname=$lastname;
			return $this;
		}
		public function setEmail(string $mail) : RegisterModel
		{
			$this->mail=$mail;
			return $this;
		}
		public static function create() : RegisterModel
		{
			return new RegisterModel();
		}
	}
}
