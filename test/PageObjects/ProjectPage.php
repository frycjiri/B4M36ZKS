<?php

namespace PageObjects
{
	/**
	 * ProjectPage short summary.
	 *
	 * ProjectPage description.
	 *
	 * @version 1.0
	 * @author Fry
	 */
	class ProjectPage extends Page
	{
		public function getTitle() : string
		{
			return $this->Webdriver()->findElement(\Facebook\WebDriver\WebDriverBy::cssSelector('div#header h1'))->getText();
		}
	}
}