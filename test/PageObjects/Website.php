<?php

namespace PageObjects
{
	include_once(__DIR__.'/LoginPage.php');
	include_once(__DIR__.'/RegisterPage.php');
	include_once(__DIR__.'/ProjectPage.php');
	include_once(__DIR__.'/ProjectListPage.php');
	/**
	 * Website short summary.
	 *
	 * Website description.
	 *
	 * @version 1.0
	 * @author Fry
	 */
	class Website
	{
		protected $webDriver;

		private $_logged_in=false;
		private $_baseurl;
		private $_staticMapping=['login'=>'{base}/login','register'=>'{base}/account/register','project'=>'{base}/projects/{id}','project_list'=>'{base}/projects'];

		public function __construct(string $baseurl)
		{
			$this->_baseurl=$baseurl;
			$this->webDriver = \Facebook\WebDriver\Remote\RemoteWebDriver::create('http://selenium__standalone-chrome:4444/wd/hub', \Facebook\WebDriver\Remote\DesiredCapabilities::chrome());
		}
		public static function create(string $base_url) : Website
		{
			return new Website($base_url);
		}

		public function LoginPage() : LoginPage
		{
			return new LoginPage($this,str_replace('{base}',$this->_baseurl,$this->_staticMapping['login']));
		}
		public function RegisterPage() : RegisterPage
		{
			return new RegisterPage($this,str_replace('{base}',$this->_baseurl,$this->_staticMapping['register']));
		}
		public function ProjectListPage() : ProjectListPage
		{
			return new ProjectListPage($this,str_replace('{base}',$this->_baseurl,$this->_staticMapping['project_list']));
		}
		public function ProjectPage(string $project_id) : ProjectPage
		{
			return new ProjectPage($this,str_replace('{id}',$project_id,str_replace('{base}',$this->_baseurl,$this->_staticMapping['project'])));
		}
		public function logout()
		{
			$this->webDriver->get($this->_baseurl.'/logout');
		}


		public function Webdriver() : \Facebook\WebDriver\Remote\RemoteWebDriver
		{
			return $this->webDriver;
		}
	}
	class Page
	{
		private $_website,$_url;
		public function __construct(Website $website,?string $url=null)
		{
			$this->_website=$website;
			$this->Webdriver()->get($url);
			$this->_url=$url;
		}
		public function Website() : Website
		{
			return $this->_website;
		}
		public function Webdriver() : \Facebook\WebDriver\Remote\RemoteWebDriver
		{
			return $this->_website->Webdriver();
		}
		protected function elementExist($by) :bool
		{
			$els = $this->Webdriver()->findElements($by);
			return count($els)!=0;
		}
		protected function inputText($selector,$text)
		{
			$this->Webdriver()->findElement(\Facebook\WebDriver\WebDriverBy::cssSelector($selector))->click()->sendKeys($text);
		}
	}
}