<?php
include_once(__DIR__.'/PageObjects/Website.php');
use \PageObjects\RegisterModel;
use \PageObjects\LoginModel;

class Homework2Test extends PHPUnit\Framework\TestCase
{
	protected $website;
	public static $username;
	public static $password;
    public static function setUpBeforeClass()
	{
		self::$password =self::rand();
		self::$username =self::rand();
	}
	protected function setUp()
	{
		$this->website= new PageObjects\Website('http://demo.redmine.org');
	}
	private static function rand() :string
	{
		$string = '';
		$characters='abcdefghijklmnopqrstuvwxyz0123456789';
		$max = strlen($characters) - 1;
		for ($i = 0; $i < 12; $i++) {
			$string .= $characters[mt_rand(0, $max)];
		}
		return $string;
	}
	private static function randEmail() :string
	{
		return self::rand().'@example.com';
	}
	protected function tearDown()
	{
		$this->website->logout();
	}

	public function testRegister()
	{
		$output=$this->website->RegisterPage()->register(
			RegisterModel::create()
			->setEmail(self::randEmail())
			->setFirstName(self::rand())
			->setLastName(self::rand())
			->setPassword(self::$password)
			->setUsername(self::$username)
		);
		self::assertTrue($output);
	}
	/** @depends testRegister */
	public function testRepeatRegister()
	{
		$output=$this->website->RegisterPage()->register(
			RegisterModel::create()
			->setEmail($this->randEmail())
			->setFirstName(self::rand())
			->setLastName(self::rand())
			->setPassword(self::$password)
			->setUsername(self::$username)
		);
		self::assertFalse($output);
	}
	/** @depends testRegister */
	public function testLogin_Positive()
	{
		$output=$this->website->LoginPage()->login(LoginModel::create()->setUsername(self::$username)->setPassword(self::$password));
		self::assertTrue($output);
	}
	/** @depends testRegister */
	public function testLogin_Negative()
	{
		$output=$this->website->LoginPage()->login(LoginModel::create()->setUsername(self::$username)->setPassword(self::$password.'ttt'));
		self::assertFalse($output);
	}
	/** @depends testRegister */
	public function testThirdProject()
	{
		$output=$this->website->LoginPage()->login(LoginModel::create()->setUsername(self::$username)->setPassword(self::$password));
		self::assertTrue($output);
		$page=$this->website->ProjectListPage();
		self::assertFalse($page->project_count()<3);
		$project_detail=$page->get_project(3);
		self::assertEquals($project_detail->open()->getTitle(),$project_detail->name);

	}

	protected function inputText($selector,$text)
	{
        $this->webDriver->findElement(Facebook\WebDriver\WebDriverBy::cssSelector($selector))->click()->sendKeys($text);
	}

    protected function assertElementFound($by)
    {
        $els = $this->webDriver->findElements($by);
        if (count($els)==0) {
            $this->fail("Element was not found");
        }
        $this->assertTrue(true);
    }
    protected function assertElementNotFound($by)
    {
        $els = $this->webDriver->findElements($by);
        if (count($els)) {
            $this->fail("Element found");
        }
        $this->assertTrue(true);
    }
}