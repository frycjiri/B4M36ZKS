<?php

class Homework1Test extends PHPUnit\Framework\TestCase
{
	protected $driver;

	protected $host='http://172.17.0.3:80/';
	protected $usr_file='app/users.ini';
	protected function setUp()
	{
		if (!$this->hasDependencies())
		{
			unlink($this->usr_file);
			touch($this->usr_file);
		}
        $this->webDriver = Facebook\WebDriver\Remote\RemoteWebDriver::create('http://selenium__standalone-chrome:4444/wd/hub', \Facebook\WebDriver\Remote\DesiredCapabilities::chrome());
	}
	protected function tearDown()
	{
		$this->webDriver->get($this->host.'?a=logout');
	}

	public function testTitle()
	{
		$this->webDriver->get($this->host);
		self::assertEquals($this->webDriver->getTitle(),'Localhost login/register test');
	}
	public function testRegister()
	{
		$this->webDriver->get($this->host);
        $this->assertElementFound(Facebook\WebDriver\WebDriverBy::id('register-form'));
		$this->inputText('#register-form input[name="username"]','roor');
		$this->inputText('#register-form input[name="password"]','toor');
        $this->webDriver->getKeyboard()->pressKey(Facebook\WebDriver\WebDriverKeys::ENTER);

        $this->assertElementNotFound(Facebook\WebDriver\WebDriverBy::id('register-form'));
        $this->assertElementNotFound(Facebook\WebDriver\WebDriverBy::id('login-form'));
        $this->assertElementFound(Facebook\WebDriver\WebDriverBy::linkText('Logout'));
	}
	/** @depends testRegister */
	public function testLogin_Positive()
	{
		$this->webDriver->get($this->host);
        $this->assertElementFound(Facebook\WebDriver\WebDriverBy::id('login-form'));
		$this->inputText('#login-form input[name="username"]','roor');
		$this->inputText('#login-form input[name="password"]','toor');
        $this->webDriver->getKeyboard()->pressKey(Facebook\WebDriver\WebDriverKeys::ENTER);

        $this->assertElementNotFound(Facebook\WebDriver\WebDriverBy::id('login-form'));
        $this->assertElementNotFound(Facebook\WebDriver\WebDriverBy::id('register-form'));
        $this->assertElementFound(Facebook\WebDriver\WebDriverBy::linkText('Logout'));

	}
	/** @depends testRegister */
	public function testLogin_Negative()
	{
		$this->webDriver->get($this->host);
        $this->assertElementFound(Facebook\WebDriver\WebDriverBy::id('login-form'));
		$this->inputText('#login-form input[name="username"]','roor');
		$this->inputText('#login-form input[name="password"]','badpassword');
        $this->webDriver->getKeyboard()->pressKey(Facebook\WebDriver\WebDriverKeys::ENTER);

        $this->assertElementFound(Facebook\WebDriver\WebDriverBy::id('login-form'));
        $this->assertElementFound(Facebook\WebDriver\WebDriverBy::id('register-form'));
        $this->assertElementNotFound(Facebook\WebDriver\WebDriverBy::linkText('Logout'));
	}

	protected function inputText($selector,$text)
	{
        $this->webDriver->findElement(Facebook\WebDriver\WebDriverBy::cssSelector($selector))->click()->sendKeys($text);
	}

    protected function assertElementFound($by)
    {
        $els = $this->webDriver->findElements($by);
        if (count($els)==0) {
            $this->fail("Element was not found");
        }
        $this->assertTrue(true);
    }
    protected function assertElementNotFound($by)
    {
        $els = $this->webDriver->findElements($by);
        if (count($els)) {
            $this->fail("Element found");
        }
        $this->assertTrue(true);
    }
}