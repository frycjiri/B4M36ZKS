<?php
session_start();
$users=parse_ini_file('users.ini');
function arr2ini(array $a)
{
    $out = '';
    foreach ($a as $k => $v)
		$out .= "$k=\"$v\"" . PHP_EOL;
    return $out;
}
if(isset($_GET["a"])&& ($_GET["a"]=="logout" || (isset($_POST["username"])&& isset($_POST["password"]))))
{
	switch($_GET["a"])
	{
		case "login":
			if(key_exists($_POST["username"],$users) && $users[$_POST["username"]]==crypt($_POST["password"],$users[$_POST["username"]]))
			{
				$_SESSION["user"]=$_POST["username"];
				$msg="Logged in.";
			}
			else
				$msg="Wrong password.";
			break;
		case "register":
			$usr=$_POST["username"];
			if(!key_exists($_POST["username"],$users))
			{
				$users[$usr]=crypt($_POST["password"]);
				file_put_contents('users.ini',arr2ini($users));
				$_SESSION["user"]=$usr;
				$msg="Registered.";
			}
			else
				$msg="User already exist.";
			break;
		case "logout":
			unset($_SESSION["user"]);
			break;
	}
}
if(isset($msg)) print("<div id='message'>{$msg}</div>");
if(isset($_SESSION["user"])) print("<a href='index.php?a=logout'>Logout</a>");
else {
?>
<title>Localhost login/register test</title>
<form id="register-form" method="post" action="index.php?a=register">
	<input type="text" name="username" placeholder="Username" />
	<input type="password" name="password" placeholder="Password" />
	<input type="submit" value="Register" />
</form>
<form id="login-form" method="post" action="index.php?a=login">
	<input type="text" name="username" placeholder="Username" />
	<input type="password" name="password" placeholder="Password" />
	<input type="submit" value="Login" />
</form>
<?php
}
?>
